# Source local ROS2 distro
  is_bionic=$(cat /etc/issue.net | grep 20)

  if [ -z "$is_bionic" ]
  then
      source /opt/ros/eloquent/setup.bash
  else
      source /opt/ros/foxy/setup.bash
  fi

cd ../epd_msgs/
rm -r build/ install/ log/
echo "Build and Sourcing epd_msgs dependency."
colcon build && source install/setup.bash

cd ../picking_scheduler/
rm -r build/ install/ log/
echo "Build and Sourcing picking_scheduler."
colcon build && source install/setup.bash
