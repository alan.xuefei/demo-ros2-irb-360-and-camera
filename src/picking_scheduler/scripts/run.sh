# Source local ROS2 distro
is_bionic=$(cat /etc/issue.net | grep 20)

if [ -z "$is_bionic" ]
then
    source /opt/ros/eloquent/setup.bash
else
    source /opt/ros/foxy/setup.bash
fi

source ../epd_msgs/install/setup.bash
source install/setup.bash

read -p "Run with sim_robot? [y/n]: " input

if [[ $input == "y" ]]; then
    use_sim_robot="true"
elif [[ $input == "n" ]]; then
    use_sim_robot="false"
fi

unset input

ros2 run picking_scheduler picking_scheduler --ros-args -p sim_robot:=$use_sim_robot
