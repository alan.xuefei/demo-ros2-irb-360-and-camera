// Copyright 2020 ROS-Industrial Consortium Asia Pacific
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <chrono>
#include <cstdlib>
#include <memory>
#include <thread>

#include "rclcpp/rclcpp.hpp"
#include "epd_msgs/srv/trigger.hpp"
#include "epd_msgs/srv/epd_object_localization_service.hpp"
#include "epd_msgs/msg/epd_object_localization.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"

#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

// Conveyor assumed to be travelling at 44mm per second.
#define CONVEYOR_LINEAR_VELOCITY 44
// Camera FOV assumed to cover only 505mm of Conveyor.
#define CONVEYOR_FOV_SEGMENT 505
// It takes 10100 milliseconds to cover one CONVEYOR_FOV_SEGMENT
#define CONVEYOR_FOV_SEGMENT_TIME 11477
// It takes approximately 10 milliseconds for a picking action to complete.
#define ROBOT_PICKING_TIME 800
// Camera is 1045mm away from the Robot Base in Robot negative x-axis.
#define ROBOT_BASE_X_OFFSET 1243
// Camera is 175mm away from the Robot Base in Robot positive y-axis.
#define ROBOT_BASE_Y_OFFSET 315
// Camera is 700mm away from the Robot Base in Robot negative x-axis.
#define ROBOT_BASE_Z_OFFSET 700

#define SERVERIP "192.168.125.1"
#define PORT 5000
#define MINY -515
#define MAXY -161
#define MINX -223
#define MAXX 430

using namespace std::chrono;
using namespace std::chrono_literals;

class PickingScheduler : public rclcpp::Node
{
public:
  PickingScheduler()
  : Node("picking_scheduler")
  {
    // Creating subscriber
    subscriber_ = this->create_subscription<epd_msgs::msg::EPDObjectLocalization>(
      "/processor/epd_localize_output",
      10,
      std::bind(&PickingScheduler::localize_callback, this, std::placeholders::_1));

    client_ = this->create_client<epd_msgs::srv::Trigger>("localize_obj");

    this->declare_parameter("sim_robot");
    rclcpp::Parameter bool_param = this->get_parameter("sim_robot");
    sim_robot = bool_param.as_bool();

    std::chrono::milliseconds service_interval;
    if (!sim_robot) {
      service_interval = std::chrono::milliseconds(CONVEYOR_FOV_SEGMENT_TIME);
    } else {
      service_interval = std::chrono::milliseconds(5000);
    }

    RCLCPP_INFO(this->get_logger(), "Issuing Localize Object Request every %dms", service_interval.count());
    timer_ = this->create_wall_timer(
        service_interval, std::bind(&PickingScheduler::queue_async_request, this));

    // Connect to ABB IRB-360 Robot.
    if (!sim_robot) {
      if (tcpConnect()) {
        RCLCPP_INFO(this->get_logger(), "[ REAL_ROBOT ] - Successful tcpConnect().");
      } else {
        RCLCPP_WARN(this->get_logger(), "[ REAL_ROBOT ] - Failed tcpConnect().");
      }
    } else {
      RCLCPP_WARN(this->get_logger(), "[ SIM_ROBOT ] - Skipping tcpConnect().");
    }

  }

  // A callback function that reads localization outputs
  void localize_callback(const epd_msgs::msg::EPDObjectLocalization::SharedPtr msg)
  {
    RCLCPP_INFO(this->get_logger(), "[ RECEIVED ] - Localization Result #%d", result_no);
    result_no++;

    if (msg->objects.size() != 0) {
      RCLCPP_INFO(this->get_logger(), "Picking [ %d ] objects.", msg->objects.size());
    } else {
      RCLCPP_WARN(this->get_logger(), "No object to pick.");
      pickingDoneInPrevousSnapshot = false;
      return;
    }

    // Create a struct that pairs PoseStamped and int together
    // so that the sorting keeps the delay and PoseStamped properly paired.
    std::vector<pick_t> picks;
    std::vector<std::pair<geometry_msgs::msg::PoseStamped,int>> picking_order;
    //Iterate through vector of detected objects to populate picking schedule
    for (size_t i = 0; i < msg->objects.size(); i++) {

      std::pair<geometry_msgs::msg::PoseStamped,int> picking_obj;

      // Converting from meters to millimeters
      picking_obj.first.pose.position.x = msg->objects[i].pos.pose.position.x * 1000;
      picking_obj.first.pose.position.y = msg->objects[i].pos.pose.position.y * 1000;
      // picking_obj.pose.position.z = msg->objects[i].pos.pose.position.z * 1000;

      // Translate the new centroid relative to the robot base position.
      picking_obj.first.pose.position.x = (-1 * picking_obj.first.pose.position.x) + ROBOT_BASE_X_OFFSET;
      picking_obj.first.pose.position.y = picking_obj.first.pose.position.y - ROBOT_BASE_Y_OFFSET;
      picking_obj.first.pose.position.z = -1094.5; // Value derived from Physical robot calibration.

      picking_obj.first.pose.position.x = picking_obj.first.pose.position.x - (2 * CONVEYOR_FOV_SEGMENT);

      int pickTimeMs = 2 * CONVEYOR_FOV_SEGMENT_TIME - msg->process_time;
      picking_obj.second = pickTimeMs - (pickingDoneInPrevousSnapshot * CONVEYOR_FOV_SEGMENT_TIME)
                                      - (!pickingDoneInPrevousSnapshot * 3000)
                                      - (pickingDoneInPrevousSnapshot * 500);

      picking_order.push_back(picking_obj);

    }

    // Sort picking order from highest x value in Camera to biggest y value.
    // Those that are closest to the end-effector should be picked first to prevent flipping.
    std::sort(picking_order.begin(), picking_order.end(), XPickComparator());
    // for (size_t i = 0; i < picking_order.size(); i++) {
    //   RCLCPP_INFO(this->get_logger(), " OLD - [ x = %0.1f ] [ y = %0.1f ]", picking_order[i].first.pose.position.x, picking_order[i].first.pose.position.y);
    //   // RCLCPP_INFO(this->get_logger(), " delay = %d", picking_order[i].second);
    // }
    // Categorize into discrete histograms
    // std::vector< std::vector< std::pair<geometry_msgs::msg::PoseStamped,int> > > picking_histogram;
    // std::vector<std::pair<geometry_msgs::msg::PoseStamped,int>> picking_column;
    //
    // for (size_t i = 0; i < picking_order.size(); i++) {
    //
    //
    //
    //   if (picking_column.size() == 0 ) {
    //     picking_column.push_back(picking_order[i]);
    //     continue;
    //   } else {
    //     if (i == 5) {
    //       RCLCPP_WARN(rclcpp::get_logger("rclcpp"), "picking_column[i-1].first.pose.position.x = %0.2f", picking_column[i-1].first.pose.position.x);
    //       RCLCPP_WARN(rclcpp::get_logger("rclcpp"), "picking_order[i].first.pose.position.x = %0.2f", picking_order[i].first.pose.position.x);
    //       RCLCPP_WARN(rclcpp::get_logger("rclcpp"), "diff = %0.2f", std::abs(picking_order[i].first.pose.position.x - picking_column[i-1].first.pose.position.x));
    //     }
    //
    //     if (std::abs(picking_order[i].first.pose.position.x - picking_column[i-1].first.pose.position.x) <= 25 ) {
    //       picking_column.push_back(picking_order[i]);
    //     } else {
    //
    //       std::sort(picking_column.begin(), picking_column.end(), YPickComparator());
    //
    //       std::vector<std::pair<geometry_msgs::msg::PoseStamped,int>> temp = picking_column;
    //       picking_histogram.push_back(temp);
    //
    //       picking_column.clear();
    //       // picking_column.push_back(picking_order[i]);
    //       auto it = picking_column.begin();
    //       picking_column.insert(it, picking_order[i]);
    //       // picking_column.emplace_back(picking_order[i]);
    //     }
    //   }
    // }

    // for (size_t i = 0; i < picking_histogram.size(); i++) {
    //   for (size_t j = 0; j < picking_histogram[i].size(); j++) {
    //       RCLCPP_INFO(this->get_logger(), " [ x = %0.1f ] [ y = %0.1f ]", picking_histogram[i][j].first.pose.position.x, picking_histogram[i][j].first.pose.position.y);
    //   }
    //   RCLCPP_INFO(this->get_logger(), " ");
    // }
    // Iterate through picking order.
    for (size_t i = 0; i < picking_order.size(); i++) {

      // SAFETY
      // Check if picking position is within safety boundaries.
      if (picking_order[i].first.pose.position.y >= MAXY || picking_order[i].first.pose.position.y <= MINY ) {
          RCLCPP_WARN(rclcpp::get_logger("rclcpp"), "Outside y-boundaries. Invalid Picking Request.");
          RCLCPP_INFO(this->get_logger(), "[ y-value ] = %0.2f", picking_order[i].first.pose.position.y);
          continue;
      } else {

        int pickTimeMs;
        if ( i == 0 ) {
          pickTimeMs = picking_order[i].second;
          // RCLCPP_INFO(this->get_logger(), " pickTimeMs = %d", pickTimeMs);
        } else {
          pickTimeMs = 0;
        }
        // RCLCPP_INFO(this->get_logger(), " [ x = %0.1f ] [ y = %0.1f ]", picking_order[i].first.pose.position.x, picking_order[i].first.pose.position.y);
        int x = static_cast<int>(picking_order[i].first.pose.position.x - (i * CONVEYOR_LINEAR_VELOCITY) - (i * 10));
        int y = static_cast<int>(picking_order[i].first.pose.position.y);

        if (x >= MAXX || x <= MINX ) {
            RCLCPP_WARN(rclcpp::get_logger("rclcpp"), "Outside x-boundaries. Invalid Picking Request.");
            RCLCPP_INFO(this->get_logger(), "[ x-value ] = %d", x);
            continue;
        }

        RCLCPP_INFO(this->get_logger(), " [pickTimeMs = %d ] [ x = %d ] [ y = %d ]", pickTimeMs, x, y);
        pick_t pick = {pickTimeMs, x, y};
        picks.push_back(pick);

      }

    }

    // Send picking request to Robot.
    if (!sim_robot) {
      if (!picks.empty()){
        RCLCPP_INFO(this->get_logger(), "[ SENDING ] - Pick Action.");
        std::thread th(&PickingScheduler::pickAll, this, picks);
        th.detach();
      } else {
      RCLCPP_WARN(this->get_logger(), "[ SIM_ROBOT ] - Why not qualified picks in this scan?");
      }
    }

    if (msg->objects.size() != 0) {
      pickingDoneInPrevousSnapshot = true;
    }

  }

  void queue_async_request()
  {
    while (!client_->wait_for_service(1s)) {

      if (!rclcpp::ok()) {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
        return;
      }
      RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Looking for easy_perception_deployment ROS package...");

    }

    auto request = std::make_shared<epd_msgs::srv::Trigger::Request>();

    using ServiceResponseFuture =
      rclcpp::Client<epd_msgs::srv::Trigger>::SharedFuture;

    auto response_received_callback = [this](ServiceResponseFuture future) {
        auto result = future.get();
    };

    RCLCPP_INFO(this->get_logger(), "[ SENDING ] - Localize Object Request #%d", request_no);
    request_no++;
    auto future_result = client_->async_send_request(request, response_received_callback);
  }

private:
    rclcpp::Client<epd_msgs::srv::Trigger>::SharedPtr client_;
    rclcpp::Subscription<epd_msgs::msg::EPDObjectLocalization>::SharedPtr subscriber_;
    rclcpp::TimerBase::SharedPtr timer_;
    int sock = 0;
    int request_no = 0;
    int result_no = 0;
    bool sim_robot;
    bool pickingDoneInPrevousSnapshot = false;

    struct pick_t {
      int delay_time_ms;
      int robot_pick_x_mm;
      int robot_pick_y_mm;
    };

    struct XPickComparator
    {
      // Compare 2 geometry_msgs::msg::PoseStamped objects using position.y values
      bool operator ()(const std::pair<geometry_msgs::msg::PoseStamped,int> & pick1,
                       const std::pair<geometry_msgs::msg::PoseStamped,int> & pick2)
      {
          if(pick1.first.pose.position.x >= pick2.first.pose.position.x) {
              return false;
          } else {
              return true;
          }
      }
    };

    struct YPickComparator
    {
      // Compare 2 geometry_msgs::msg::PoseStamped objects using position.y values
      bool operator ()(const std::pair<geometry_msgs::msg::PoseStamped,int> & pick1,
                       const std::pair<geometry_msgs::msg::PoseStamped,int> & pick2)
      {
          if(pick1.first.pose.position.y >= pick2.first.pose.position.y) {
              return true;
          } else {
              return false;
          }
      }
    };

    bool tcpConnect()
    {
      struct sockaddr_in serv_addr;

      if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
      {
         printf("\n Socket creation error \n");
         return false;
      }

      serv_addr.sin_family = AF_INET;
      serv_addr.sin_port = htons(PORT);

      // Convert IPv4 and IPv6 addresses from text to binary form
      if(inet_pton(AF_INET, SERVERIP, &serv_addr.sin_addr)<=0)
      {
         printf("\nInvalid address/ Address not supported \n");
         return false;
      }

      if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
      {
         printf("\nConnection Failed\n");
         printf("Oh dear, something went wrong with read()! %s\n", strerror(errno));
         return false;
      }

      char buffer[1024] = {0};
      char const *pchar_s = "test";
      send(sock , pchar_s, strlen(pchar_s), 0);
      read(sock , buffer, 1024);

      return true;
    }

    std::mutex mtx;

    void pickAll(const std::vector<pick_t> picks)
    {
        mtx.lock();
        RCLCPP_INFO(this->get_logger(), "start to pick all for this scan %d \n", picks.size());

        for (auto & pick : picks) {

          RCLCPP_INFO(this->get_logger(), "[ pickObject() ] = move to the position x %d, y %d and wait for %d ms",
                    pick.robot_pick_x_mm, pick.robot_pick_y_mm, pick.delay_time_ms);

          std::string s_x = std::to_string(pick.robot_pick_x_mm);
          char const *pchar_x = s_x.c_str();
          std::string s_y = std::to_string(pick.robot_pick_y_mm);
          char const *pchar_y = s_y.c_str();
          std::string s_t = std::to_string(pick.delay_time_ms);
          char const *pchar_t = s_t.c_str();

          high_resolution_clock::time_point t1 = high_resolution_clock::now();

          char buffer[4] = {0};
          send(sock , pchar_x, strlen(pchar_x), 0);
          read(sock , buffer, 1024);

          send(sock , pchar_y, strlen(pchar_y), 0);
          read(sock , buffer, 1024);

          send(sock , pchar_t, strlen(pchar_t), 0);

          high_resolution_clock::time_point t2 = high_resolution_clock::now();
          duration<double, std::milli> time_span = t2 - t1;
          RCLCPP_INFO(this->get_logger(), "[ pickObject() ] = took %f ms to send x y t",  time_span.count());

          read(sock , buffer, 1024);

          if (strcmp(buffer, "Done") != 0 ){
            RCLCPP_INFO(this->get_logger(), "[ pickObject() ] = Not Done took %f ms buffer %s",  time_span.count(), buffer);
          } else{
          RCLCPP_INFO(this->get_logger(), "[ pickObject() ] = Pick and Place took %f ms",  time_span.count());
          }
        }

      RCLCPP_INFO(this->get_logger(), "[ pickObject() ] = start to pick all for this scan end");
      mtx.unlock();

    }
};
