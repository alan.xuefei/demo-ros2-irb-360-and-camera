
# **tds_unimate_demo_2020**

## **What Is This?**

This repository contains codes responsible for the 2 nodes used in the demonstration, **tds_unimate_demo_2020**:

1. picking_scheduler
2. tcp_package

#### picking_scheduler
This node sends a trigger service to **easy_perception_deployment**, obtains the Precision-Level 3 Localization results and sending picking request to **tcp_package**.

#### tcp_package
This node receives picking request `geometry_msgs::msg::PoseStamped` from **picking_scheduler** and sends a TCP message to Robot to execute a picking action.


## **Dependencies**

#### **Hardware**
1. Intel Realsense D415 Stereoscopic Camera (Camera)
2. Quadro M1200 (GPU)
3. ABB IRB-360 (Robot)

#### **Software**
1. Docker
2. Nvidia CUDA 10.2, CUDNN 7.6.5
3. ROS2 Eloquent

## **Setup**
Under this section, follow the instructions below to setup the following 4 components.

1. Camera - **[ros2_intel_realsense](https://github.com/intel/ros2_intel_realsense/tree/eloquent)**
2. Perception - **[easy_perception_deployment](https://gitlab.com/ROSI-AP/rosi-ap_rect/easy_perception_deployment/-/tree/milestone2_dev)**
3. Manipulation & Robot - **[picking_scheduler & tcp_package](https://gitlab.com/alan.xuefei/demo-ros2-irb-360-and-camera)**

#### Camera
Follow steps to install ROS2 packages that interfaces the Intel Realsense D415.

```bash
cd $HOME
mkdir -p demo_ws/src && cd demo_ws/src
git clone https://github.com/intel/ros2_intel_realsense.git --branch eloquent --single-branch --depth 1
cd ../
source /opt/ros/eloquent/setup.bash
colcon build
```

#### Perception
Follow steps to install ROS2 package that provides Perception capability.

```bash
cd $HOME/demo_ws/src
git clone https://gitlab.com/ROSI-AP/rosi-ap_rect/easy_perception_deployment
cd easy_perception_deployment/easy_perception_deployment
git reset --hard f36300a7
./run.bash
# Press Deploy
# Configure (Choose Model, Choose Label List, Toggle Action, Choose Use Case:Localization, Choose GPU)
# Press Run
# Wait for 30 minutes while it builds everything and run the package.
```

#### Manipulation & Robot
Follow setups to install ROS2 packages that provides Manipulation and Robot capability.

```bash
cd $HOME/demo_ws/src
git clone https://gitlab.com/alan.xuefei/demo-ros2-irb-360-and-camera.git
cd ../
source /opt/ros/eloquent/setup.bash
colcon build
```
